#!/usr/bin/env python3

import hello

def test_hello():
    """ test unitaitre qui vérifie le retour de la fonction hello() """
    # assertion error
    assert hello.hello() == "hello world"
    # test unitaire qui passe avec succès
    assert hello.hello() == "Hello World !"


