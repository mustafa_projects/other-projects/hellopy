#!/usr/bin/env python3
""" module hello """

# ceci est un commentaire comme en bash

#def hello():
#    """ procédure qui ne retrourne rien mais affiche hello world """
#    print("Hello World !")

# appel de la procédure
#hello()

def hello():
    """ fonction qui retourne hello world """
    return "Hello World !"

if __name__ == "__main__":
    salut = hello()
    print(salut)

